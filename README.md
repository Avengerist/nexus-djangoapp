If you are using http and want to run this successfully you need to add in /etc/docker/daemon.json:
```
{
    "insecure-registries" : [ "remote_repository:port" ]
}
```
Also change IP address in Dockerfile for remote proxy, which will contain basic image
