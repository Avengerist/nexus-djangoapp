FROM 10.10.99.56:8082/python:3.10.7-alpine
EXPOSE 8000
WORKDIR /var/www/django-girls
COPY app/ .
RUN pip install -r requirements.txt
RUN mkdir ./db
CMD python manage.py migrate && sh init.sh && python manage.py runserver 0.0.0.0:8000
